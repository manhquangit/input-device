# input-device ([for Vue](https://gitlab.com/manhquangit/input-device))

**input-device**
This plugin supported when you change select language keyboard (Eg: Japanese text, Korean text, Vietnamese text, English text, ...) will error with html tag input of number and string (that use maxlength and number), this error it still will show character special

If you need support, you can push request and question (stackoverflow, ...)

- with version 1.0.7, i supported
    - input number (device ipad, desktop, mobile)
    - input string (device ipad, desktop, mobile)

TODO version 2.0:

- [x] input date picker
- [x] input month picker

TODO version 3.0:

### Install

```cmd
npm install input-device --save --save-dev
or
yarn add input-device
```

### Quick Start

```js
import InputDevice from "input-device/src/components/InputDevice.vue"
```

## Props
- type: text, integer, number
- placeholder: your variable
- v-model: your variable (it will auto update value)
- font-message: your font message
- custom-class: your css class
- maxlength: your maxlength
- message: your message (default message error)
- @input: event when input change value
- @handle-blur: event when blur input
- rounded: decimal value after dot
- disabled: is boolean true or false when it need disabled input
- show-format-comma: is boolean true or false when it need show value has comma
- show-dot-decimal: is boolean true or false when it need show decimal .00 with number int is decimal
- show-clear-input: is show icon x use it to clear value input
<!-- END -->
Thank You for use it