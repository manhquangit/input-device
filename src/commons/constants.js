export const TYPE_INT = 'integer';

export const TYPE_DECI = 'decimal';

export const TYPE_STR = 'text';

export const TYPE_DATE = 'datepicker';

export const TYPE_MONTH = 'monthpicker';

export const MAX_LEN_DEFAULT = 100;

export const BACK_KEY = 'Backspace';
export const DELETE_TYPE = 'deleteContentBackward';

export const ROUNDED_DEFAULT = 2;

export const JP_NUMBER = [
    'ﾇ',
    'Ì',
    'ｱ',
    'ｳ',
    'ｴ',
    'ｵ',
    'Ô',
    'Õ',
    'ﾖ',
];

export const CENTER_POSITION = 0;
export const LEFT_POSITION = -1;
export const RIGHT_POSITION = 1;

export const LIST_INT = ['integer', 'datepicker', 'monthpicker'];
export const LIST_NUMBER = ['integer', 'datepicker', 'monthpicker', 'decimal'];
