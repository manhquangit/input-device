import {
  JP_NUMBER,
  LEFT_POSITION,
  CENTER_POSITION,
  RIGHT_POSITION,
} from './constants';

export const getOs = () => {
    let OS = 'Unknown';
    const isIpad = /Macintosh/i.test(navigator.userAgent) && navigator.maxTouchPoints && navigator.maxTouchPoints > 1;
    const ua = navigator.userAgent;
    if (ua.indexOf('Win') !== -1) {
      OS = 'windows';
    } else if (ua.indexOf('X11') !== -1) {
        OS = 'unix';
    } else if (ua.indexOf('Linux') !== -1) {
        OS = 'linux';
    } else if (/(tablet|ipad|playbook|silk)|(android(?!.*mobi))/i.test(ua) || isIpad) {
      OS = 'tablet';
    } else if (
      /Mobile|iP(hone|od)|Android|BlackBerry|IEMobile|Kindle|Silk-Accelerated|(hpw|web)OS|Opera M(obi|ini)/.test(ua)
    ) {
      OS = 'mobile';
    } else if (ua.indexOf('Mac') !== -1) {
      OS = 'macos';
    }
    return OS;
};

export const isNumberKeyPress = (value) => {
  const indexKey = JP_NUMBER.indexOf(value);
  const isNumberJp = indexKey > -1;
  return /^\d+$/.test(value) || isNumberJp;
};

export const getValKeyPress = (keyCode) => {
  let value = keyCode;
  const indexKey = JP_NUMBER.indexOf(value);
  const isNumberJp = indexKey > -1;
  if (isNumberJp) {
      value = indexKey + 1;
  }

  return value;
};

export const getPositionMouse = (selectionStart, selectionEnd) => {
  let position = '';
  if (selectionEnd < selectionStart) {
      position = LEFT_POSITION;
  } else if (selectionEnd === selectionStart) {
      position = CENTER_POSITION;
  } else {
      position = RIGHT_POSITION;
  }

  return position;
};

export const isNotEmpty = (value) => {
  const valTmp = value.toString().trim();
  return valTmp && valTmp.length;
};
