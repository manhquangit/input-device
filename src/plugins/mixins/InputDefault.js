import BlurInput from '../directives/BlurInput';
import { LIST_NUMBER } from '../../commons/constants';
import { isNotEmpty } from '../../commons/helpers';

const ROUNDED = 2;
export default {
    props: {
        value: {
            type: [String, Number],
            default: null,
        },
        typeMessage: {
            type: String,
            default: 'error',
        },
        placeholder: String,
        type: {
            type: String,
            default: 'text',
        },
        disabled: {
            type: Boolean,
            default: false,
        },
        readonly: {
            type: Boolean,
            default: false,
        },
        showFormatComma: {
            type: Boolean,
            default: false,
        },
        showDotDecimal: {
            type: Boolean,
            default: false,
        },
        maxlength: {
            type: Number,
            default: 100,
        },
        rounded: {
            type: Number,
            default: 0,
        },
        customClass: {
            type: String,
            default: '',
        },
        message: String,
        fontMessage: {
            type: String,
            default: 'text-sm lg:text-base',
        },
        colorPlacholder: {
            type: String,
            default: 'rgb(133, 133, 133)',
        },
        colorText: {
            type: String,
            default: 'inherit',
        },
        showClearInput: {
            type: Boolean,
            default: true,
        },
    },
    data() {
        return {
            hasFocus: false,
            currentLength: 0,
            showVal: '',
            currentData: '',
        };
    },
    directives: {
        BlurInput,
    },
    methods: {
        initData() {
            if (this.value && this.value.length) {
                this.currentData = this.value.trim();
                this.showVal = this.getValInput(this.value);
                this.currentLength = this.value.trim().length;
            }
        },
        getValInput(value) {
            let currentValue = value;

            if (isNotEmpty(value) && isNotEmpty(this.type)) {
                if (LIST_NUMBER.indexOf(this.type) > -1) {
                    if (this.showFormatComma) {
                        currentValue = this.numberWithCommas(currentValue);
                    }
                    if (this.showDotDecimal && this.type === 'decimal') {
                        const numberDots = currentValue.split('.');
                        if (!(numberDots && numberDots.length >= 2)) {
                            currentValue = `${currentValue}.00`;
                        }
                    }
                }
            }

            return currentValue;
        },
        checkDisabled() {
            return this.disabled || this.readonly;
        },
        decimalFormat(event, options, delay) {
            const type = this.type;
            const rounded = this.rounded || ROUNDED;
            const MAX_LENGTH = this.maxlength;
            let partten = '';
            let isFormat = false;
            let result = '';
            if (type === 'decimal') { // allow Period character
                partten = /[^0-9.]/g;
                isFormat = true;
            } else {
                partten = /[^0-9]/g;
                isFormat = false;
            }
            try {
                let inputValue;
                if (typeof event === 'object') {
                    inputValue = event.target.value;
                } else {
                    inputValue = event;
                }
                inputValue = String(inputValue).replace(partten, '');
                result = inputValue;
                if (isFormat) {
                    const maxLength = MAX_LENGTH || 20;

                    const formattedValue = this.formatNumberValue(inputValue, delay, rounded, maxLength);
                    return formattedValue;
                }
                if (MAX_LENGTH && inputValue && inputValue.length > MAX_LENGTH) {
                    const newValue = inputValue.substr(0, MAX_LENGTH);
                    return newValue;
                }
                return result;
            } catch (error) {
                // eslint-disable-next-line no-console
                console.log(error);
            }
            return event;
        },
        numberWithCommas(numb) {
            const numbString = isNotEmpty(numb) ? numb.toString() : '';
            const regex = /\B(?=(\d{3})+(?!\d))/g;
            const ret = numbString.replace(regex, ',');
            return ret;
        },
        formatNumberValue(inputValue, delay, rounded, maxLength) {
            let formattedValue = inputValue;
            return formattedValue;
        },
    },
};
