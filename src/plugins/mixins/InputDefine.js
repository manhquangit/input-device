import BlurInput from 'input-device/src/plugins/directives/BlurInput';

const TIMEOUT = 50;
export default {
    directives: {
        BlurInput,
    },
    methods: {
        handleClearInput() {
            if (this.checkDisabled()) {
                return;
            }

            this.currentData = '';
            this.currentLength = 0;
            setTimeout(() => {
                if (!this.hasFocus) {
                    this.hasFocus = true;
                    if (this.$refs.inputRef) {
                        this.$refs.inputRef.focus();
                    }
                }
            }, TIMEOUT);
        },
        handleBlur() {
            if (this.checkDisabled()) {
                return;
            }

            if (this.hasFocus) {
                if (this.$refs.inputRef) {
                    this.$refs.inputRef.blur();
                }
                this.hasFocus = false;
                this.$emit('input', this.currentData);
                this.showVal = this.getValInput(this.currentData);
                this.$emit('check-blur');
            }
        },
        handleClick() {
            if (this.checkDisabled()) {
                return;
            }
            this.showVal = '';
            setTimeout(() => {
                this.$refs.inputRef.focus();
                this.hasFocus = true;
            }, TIMEOUT);
        },
        handleKeyUp(event) {
            if (this.checkDisabled()) {
                return;
            }
            const val = event.target.value.toString().trim();
            this.currentData = val;
            this.currentLength = val.length;
            this.$emit('input', this.currentData);
        },
    },
};
