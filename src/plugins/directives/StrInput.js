import { MAX_LEN_DEFAULT } from '../../commons/constants';

let oldValStr;

export default {
    inserted(el) {
        el.onfocus = (event) => {
          oldValStr = event.target.value;
        };
        el.oninput = (event) => {
          const value = event.target.value.toString().trim().replace(
            /[\uff01-\uff5e]/g,
            (ch) => String.fromCharCode(ch.charCodeAt(0) - 0xfee0),
          );
          let maxLength = MAX_LEN_DEFAULT;
          if (el.attributes.textlen) {
            maxLength = Number(el.attributes.textlen.nodeValue);
          }
          let quantity = '';
          if ([...value].length > maxLength) {
            quantity = oldValStr;
          } else {
            quantity = value;
          }
          quantity = quantity.toString().trim();
          el.value = quantity;
          oldValStr = quantity;
        };
    },
    update(el, binding, vnode) {
        const value = vnode.data.domProps.value;
        if (!value) {
            oldValStr = value;
        }
    },
};
