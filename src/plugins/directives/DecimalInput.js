import {
 MAX_LEN_DEFAULT, BACK_KEY, DELETE_TYPE, ROUNDED_DEFAULT,
} from '../../commons/constants';

let oldValDecimal;

export default {
    inserted(el) {
        el.onfocus = (event) => {
          oldValDecimal = event.target.value;
          el.removeAttribute('readonly');
        };
        el.oninput = (event) => {
          const value = event.target.value.toString().trim().replace(
            /[\uff01-\uff5e]/g,
            (ch) => String.fromCharCode(ch.charCodeAt(0) - 0xfee0),
          ).replace(/[^0-9.]/g, '');
          let keyCode = '';
          if (event.inputType === DELETE_TYPE) {
            keyCode = BACK_KEY;
          } else {
            keyCode = event.data;
          }
          let quantity = '';
          let rounded = ROUNDED_DEFAULT;
          let maxLength = MAX_LEN_DEFAULT;
          if (el.attributes.rounded) {
            rounded = Number(el.attributes.rounded.nodeValue);
          }
          if (el.attributes.textlen) {
            maxLength = Number(el.attributes.textlen.nodeValue);
          }
          const keyDot = '.';
          const numbers = value.split(keyDot);
          const popLastVal = value.replace(/.$/, '');
          if (keyCode === BACK_KEY) {
            quantity = value;
          } else if (numbers.length > 2) {
            quantity = popLastVal;
          } else if (numbers.length === 2 && numbers[1] && numbers[1].length > rounded) {
            quantity = oldValDecimal;
          } else if (numbers[0].length > maxLength) {
            quantity = oldValDecimal;
          } else {
            quantity = value;
          }
          quantity = quantity.toString().trim();
          el.value = quantity;
          oldValDecimal = quantity;
        };
    },
    update(el, binding, vnode) {
        const value = vnode.data.domProps.value;
        if (!value) {
            oldValDecimal = value;
        }
    },
};
