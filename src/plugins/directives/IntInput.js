import { MAX_LEN_DEFAULT, BACK_KEY, DELETE_TYPE } from '../../commons/constants';

let oldValInt;

export default {
    inserted(el) {
        el.onfocus = (event) => {
          oldValInt = event.target.value;
          el.removeAttribute('readonly');
        };
        el.oninput = (event) => {
          const value = event.target.value.toString().trim().replace(
            /[\uff01-\uff5e]/g,
            (ch) => String.fromCharCode(ch.charCodeAt(0) - 0xfee0),
          );
          let keyCode = '';
          let maxLength = MAX_LEN_DEFAULT;
          if (el.attributes.textlen) {
            maxLength = Number(el.attributes.textlen.nodeValue);
          }
          if (event.inputType === DELETE_TYPE) {
            keyCode = BACK_KEY;
          }
          let quantity = '';
          if (keyCode === BACK_KEY) {
            quantity = value;
          } else if (value.length > maxLength) {
            quantity = oldValInt;
          } else {
            quantity = value.replace(/[^0-9]/g, '');
          }
          quantity = quantity.toString().trim();
          el.value = quantity;
          oldValInt = quantity;
        };
    },
    update(el, binding, vnode) {
        const value = vnode.data.domProps.value;
        if (!value) {
            oldValInt = value;
        }
    },
};
